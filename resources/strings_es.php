<?php
/**
 * Created by PhpStorm.
 * User: Marco
 * Date: 28/05/2016
 * Time: 15:23
 */
/*Header*/
$cloudgate_index_singin = 'Accede';
$cloudgate_index_slogan = 'Algunos los llaman oponentes, yo los llamo victimas.';

/*Catalog*/
$cloudgate_index_catalog = 'Nuestro Catalogo';
$cloudgate_index_bicycle = 'Aquí aparecera más información sobre este producto.';
$cloudgate_index_slogan_two = 'Productos premium para corredores premium';

/*About Us*/
$cloudgate_index_aboutus = 'Sobre nosotros';
$cloudgate_index_foundedtitle = 'Seabright Cannery en Santa Cruz , California';
$cloudgate_index_founded = 'Cloud Gate fue fundada en 1993 en un garaje con espacio para un coche, en el Seabright Cannery en Santa Cruz, California. Nuestra primera bici la Tazmon, una doble suspensión que tenía un diseño monopivote, agitó el mercado en ese momento y nos ayudó a definir nuestros valores como "simplemente avanzada". Esa bici fue la piedra angular de una empresa que nació con la idea de hacer las cosas a nuestra manera, para conseguir los mejores resultados. Hemos estado removiendo el mercado desde entonces.';
$cloudgate_index_worktitle = 'Producimos 16 modelos de mountain bikes';
$cloudgate_index_work = 'Hoy en día producimos 16 modelos de mountain bikes, que incluyen toda una gama de mujer completa como es Juliana. Fabricadas tanto en carbono como en aluminio, cubrimos todas las necesidades desde rígidas hasta máquinas de descenso con 215mm de recorrido. Ofrecemos nuestro sistema exclusivo de suspensión VPP (Virtual Pivot Point) en todos los modelos altos de gama. Todas las bicis con sistema VPP están disponibles con un rango enorme de componentes y opciones de suspensión, además de estar ensambladas a mano en nuestros almacenes de Santa Cruz. Cada una de nuestras bicis es la síntesis de nuestro deseo de montar y construir las mountain bikes como mejor rendimiento del mundo. Ni más, ni menos.';
$cloudgate_index_committedtitle = 'Comprometidos';
$cloudgate_index_committed = 'Nos podemos poner un poco sentimentales aquí y hablar de lo comprometidos y apasionados que somos como riders y lo mucho que amamos las bicis, pero es algo demasiado habitual en las pestañas de "sobre nosotros" en las webs de compañías de bicis. TODOS los que trabajamos en esta industria debemos estar comprometidos y apasionados. Se necesita este tipo de compromiso para intentar fabricar la bici perfecta. Todavía pensamos que perseguimos ese sueño con más intensidad, inteligencia y a veces incluso rareza que los otros fabricantes.';

/*Footer*/
$cloudgate_index_currentjobtitle = 'Ofertas de empleo actuales';
$cloudgate_index_currentjob ='Gracias por su interés en Cloud Gate Bicycles ! <br> Situado en Santa Cruz, California , ofrecemos un ambiente relajado de trabajo ,locales singletrack  , beneficios excepcionales y la oportunidad de trabajar con un grupo de apasionados entusiastas de la bicicleta para tratar de construir la bici perfecta . Nuestros objetivos son crear las mejores bicicletas de montaña en el planeta.';
$cloudgate_index_resources='Recursos';
$cloudgate_index_contactus='Contacto';