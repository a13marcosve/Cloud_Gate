<?php
/**
 * Created by PhpStorm.
 * User: Marco
 * Date: 28/05/2016
 * Time: 17:05
 */
/*Header*/
$cloudgate_index_singin = 'Sing In';
$cloudgate_index_slogan = 'Some call them opponents, I call them victims.';

/*Catalog*/
$cloudgate_index_catalog = 'Our Catalog';
$cloudgate_index_bicycle = 'Here is some more information about this product that is only revealed once clicked on.';
$cloudgate_index_slogan_two = 'Premium products for premium riders.';

/*About Us*/
$cloudgate_index_aboutus = 'About Us';
$cloudgate_index_foundedtitle = 'Seabright Cannery in Santa Cruz , California';
$cloudgate_index_founded = 'Cloud Gate Bicycles was founded in 1993 in a space the size of a single car garage in the Seabright Cannery, in Santa Cruz California. Our first bike, the Tazmon, was a single pivot dual suspension bike that rattled established sensibilities and helped define our then- ethos of "simply advanced." That bike was the cornerstone for a brand that was founded on the notion of doing things our own way in order to build the best. We have been rattling sensibilities ever since. ';
$cloudgate_index_worktitle = 'Manufacture 16 models of mountain bike';
$cloudgate_index_work = 'We now manufacture 16 models of mountain bike which includes a full women\'s Juliana range.  Made from either carbon fiber or aluminum, we cover everything from hardtails to 8.5" travel downhill machines.  We offer two suspension technologies; efficient single-pivot designs on certain aluminium bikes, and our advanced VPP (Virtual Pivot Point) technology on all high end models. VPP bikes are available with a wide range of component and suspension options to choose from, each hand-assembled to order in our California factory. Every one of our bikes is the distillation of our desire to ride and build the highest performance mountain bikes in the world. No more, no less.';
$cloudgate_index_committedtitle = 'Committed';
$cloudgate_index_committed = 'We could get all touchy feely here and talk about how we are committed and impassioned riders, and how we love bikes so much that it somehow elevates us into something unique, but that\'s kind of common fodder for these "about" tabs on bike company websites. EVERYONE working in this industry should be committed and impassioned. It takes that kind of commitment to try and build the perfect bike. Still, we think we chase that vision harder, smarter and sometimes a bit weirder than the others.';

/*Footer*/
$cloudgate_index_currentjobtitle = 'Current Job Openings';
$cloudgate_index_currentjob = 'Thank you for your interest in Santa Cruz Bicycles! <br> Located in Santa Cruz, California, we offer a relaxed working environment, local singletrack, exceptional benefits and the opportunity to work with a group of bike enthusiasts impassioned to try and build the perfect bike.  Our goals are to create the best mountain bikes on the planet and have a good time doing it.';
$cloudgate_index_resources = 'Resources';
$cloudgate_index_contactus = 'Contact Us';