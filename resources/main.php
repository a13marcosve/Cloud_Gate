<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Cloud Gate Bikes</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Pacifico"/>
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<div class="navbar-fixed">
    <nav class="white" role="navigation">
        <div class="nav-wrapper container">
            <ul class="right hide-on-med-and-down">
                <!-- TODO: crear control de usuarios -->
                <li>
                    <a href="#">
                        <?php echo $cloudgate_index_singin; ?>
                    </a>
                </li>
            </ul>

            <!-- TODO: implementar enlaces no menu lateral -->
            <ul id="nav-mobile" class="side-nav">
                <li>
                    <a href="#">
                        <?php echo $cloudgate_index_singin; ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <?php echo $cloudgate_index_catalog; ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <?php echo $cloudgate_index_aboutus; ?>
                    </a>
                </li>
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>


<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div id="header" class="container">
            <h1 class="header center teal-text text-lighten-2">Cloud Gate</h1>
            <div class="row center">
                <h5 class="header col s12 light">
                    <?php echo $cloudgate_index_slogan; ?>
                </h5>
            </div>
        </div>
    </div>
    <div class="parallax">
        <video id="background_video" loop="loop" muted autoplay></video>
        <div id="video_cover"></div>
        <div id="overlay"></div>
    </div>
</div>

<div class="container">
    <div class="section">
        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>
                    <?php echo $cloudgate_index_catalog; ?>
                </h4>
            </div>
        </div>
        <!--   Icon Section   -->

        <div class="row">
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/v10_website_profile_1.jpg?itok=ntiKXQ2r?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">V10<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">V10<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/nomad_website_profile_1.jpg?itok=2Uopf48_?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Nomad<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Nomad<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/bronson_website_profile_1.jpg?itok=RzQFLMRT?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Bronson<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Bronson<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
        </div>
        <!--   Icon Section   -->

        <div class="row">
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/heckler_website_profile_1.jpg?itok=2CnJusg1?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Heckler<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Heckler<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/htprofilered_0.jpg?itok=gzo3FSaC?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Hightower<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Hightower<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
            <div class="card medium">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator"
                         src="http://www.santacruzbicycles.com/files/styles/bike_engine_listview/public/frame-thumbs/my16_jackal_profile_0_1.jpg?itok=KGuFL1i1?1464293974">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4">Jackal<i
                            class="material-icons right">more_vert</i></span>
                    <p><a href="#">This is a link</a></p>
                </div>
                <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Jackal<i
                        class="material-icons right">close</i></span>
                    <p>
                        <?php echo $cloudgate_index_bicycle; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">
                    <?php echo $cloudgate_index_slogan_two; ?>
                </h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="img/background2.jpg"></div>
</div>

<div class="container">
    <div class="section">
        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>
                    <?php echo $cloudgate_index_aboutus; ?>
                </h4>
            </div>
        </div>
    </div>
    <!--   Icon Section   -->
    <div class="row">
        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center brown-text"><i class="material-icons">location_on</i></h2>
                <h5 class="center">
                    <?php echo $cloudgate_index_foundedtitle; ?>
                </h5>

                <p class="light">
                    <?php echo $cloudgate_index_founded; ?>
                </p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center brown-text"><i class="material-icons">settings</i></h2>
                <h5 class="center">
                    <?php echo $cloudgate_index_worktitle; ?>
                </h5>

                <p class="light">
                    <?php echo $cloudgate_index_work; ?>
                </p>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="icon-block">
                <h2 class="center brown-text"><i class="material-icons">thumb_up</i></h2>
                <h5 class="center">
                    <?php echo $cloudgate_index_committedtitle; ?>
                </h5>

                <p class="light">
                    <?php echo $cloudgate_index_committed; ?>
                </p>
            </div>
        </div>
    </div>

</div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="parallax">
        <img src="img/background3.jpg">
    </div>
</div>

<footer class="page-footer teal">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">
                    <?php echo $cloudgate_index_currentjobtitle; ?>
                </h5>
                <p class="grey-text text-lighten-4">
                    <?php echo $cloudgate_index_currentjob; ?>
                </p>


            </div>
            <div class="col l3 s12">
                <h5 class="white-text">
                    <?php echo $cloudgate_index_resources; ?>
                </h5>
                <ul>
                    <li><a class="white-text" href="http://www.santacruzbicycles.com/">Bicycle Shop</a></li>
                    <li><a class="white-text" href="http://materializecss.com/">Materialize CSS</a></li>
                    <li><a class="white-text" href="https://rishabhp.github.io/bideo.js/">Bideo JS</a></li>
                    <li><a class="white-text" href="https://www.youtube.com/watch?v=xQ_IQS3VKjA">Inspiration - The
                            Ridge</a></li>
                    <li><a class="white-text" href="https://github.com/MarcosVicenteE?tab=repositories">My Gits
                            Respositories</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">
                    <?php echo $cloudgate_index_contactus; ?>
                </h5>
                <ul>
                    <li><a class="white-text" href="#">Tlf: 981 66 66 66</a></li>
                    <li><a class="white-text" href="#">Email: cloud.gate@info.net</a></li>
                    <li><a class="white-text" href="#">Adress: IES San Clemente</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="brown-text text-lighten-3" href="#">Marcos Vicente Espasandín</a>
            <a id='spanish' class="brown-text text-lighten-3" href="./index_es.php">Español</a>
            <a id='english' class="brown-text text-lighten-3" href="./index.php">English</a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/bideo.js"></script>
<script src="js/init.js"></script>
<script src="js/main.js"></script>


</body>
</html>